require_relative 'generic_employee'

class GenericChief < GenericEmployee
  attr_reader :subordinates

  def initialize(name, working_since)
    super
    @subordinates = []
  end

  def add_subordinate(s)
    @subordinates << s
    s.chief       = self
  end

  def salary
    rate + bonus + subordinates_salary
  end
end