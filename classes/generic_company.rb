class GenericCompany
  attr_accessor :employees, :name
  
  def initialize(name)
    @name      = name
    @employees = []
  end
end