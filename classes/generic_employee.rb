require 'date'
require 'active_support'
require 'active_support/core_ext'

class GenericEmployee
  attr_accessor :chief, :salary_date, :name

  def initialize(name, working_since)
    @name           = name
    @working_since  = working_since
    @chief          = nil
    @salary_date    = DateTime.now
  end

  def salary
    rate + bonus
  end

  def subordinates
    []
  end

private
  def rate
    2500
  end

  def base_bonus
    3.0
  end

  def max_bonus_percentage
    30.0
  end

  def bonus
    b = worked_years * ((base_bonus / 100) * rate)
    b > max_bonus ? max_bonus : b
  end

  def max_bonus
    max_bonus_percentage / 100 * rate
  end

  def worked_years
    (@salary_date.to_time.to_i - @working_since.to_time.to_i) / 60 / 60 / 24 / 365
  end  
end