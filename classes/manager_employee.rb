require_relative 'generic_chief'

class ManagerEmployee < GenericChief
  def subordinates_salary
    @subordinates.map(&:salary).reduce(:+).to_f * subordinates_salary_percentage / 100
  end

private

  def base_bonus
    5.0
  end

  def max_bonus_percentage
    40.0
  end

  def subordinates_salary_percentage
    0.5 
  end
end