require_relative 'generic_chief'

class SalesEmployee < GenericChief
  def subordinates_salary
    collect_sub_ordinates(self).map(&:salary).reduce(:+).to_f * subordinates_salary_percentage / 100
  end

private

  def base_bonus
    1.0
  end

  def max_bonus_percentage
    35.0
  end

  def subordinates_salary_percentage
    0.3 
  end

  def collect_sub_ordinates(employee, all = [])
    employee.subordinates.each do |subordinate|
      all << subordinate
      
      collect_sub_ordinates(subordinate, all) if subordinate.subordinates.any?
    end

    all
  end
end