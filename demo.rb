require 'colorize'

require_relative 'classes/generic_company'
require_relative 'classes/generic_employee'
require_relative 'classes/manager_employee'
require_relative 'classes/sales_employee'

company = GenericCompany.new('Apple')

employee1 = GenericEmployee.new 'Scott', DateTime.parse('2017/11/24')
employee2 = GenericEmployee.new 'Fred',  DateTime.parse('2014/04/12')

manager1  = ManagerEmployee.new 'Marla', DateTime.parse('2012/01/15')
manager1.add_subordinate employee1

manager2  = ManagerEmployee.new 'Robert', DateTime.parse('2013/08/01')
manager2.add_subordinate employee2

manager1.add_subordinate manager2

sales1    = SalesEmployee.new 'Samantha', DateTime.parse('2011/02/28')
sales1.add_subordinate manager1

company.employees = [ employee1, employee2, manager1, manager2, sales1 ]


puts 'Base employee rate is 2500'

puts "Employees salary on #{DateTime.now.strftime('%Y/%m/%d')}:".yellow
company.employees.each { |e| puts "#{e.name} - #{e.salary.round(2)}" }

puts "Total company salary is #{company.employees.map(&:salary).reduce(:+).round(2)}"

future_date = DateTime.now + 28.month
puts '========================================================='
puts "Employees salary on #{future_date.strftime('%Y/%m/%d')}:".green
company.employees.each { |e| e.salary_date = future_date; puts "#{e.name} - #{e.salary.round(2)}" }
puts "Total company salary is #{company.employees.map {|e| e.salary_date = future_date; e.salary }.reduce(:+).round(2)}"