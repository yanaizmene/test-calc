require 'spec_helper'
require_relative '../../classes/generic_employee'

RSpec.describe GenericEmployee do
  #base rate is 2500, bonus is 3% for each year, max bonus is 30% of base rate

  describe '.salary' do
    let(:employee) { described_class.new('Scott', DateTime.now - working) }      
    
    context 'employee is working for less than year + arbitrary salary date' do
      let(:working) { 0.year }
      before { employee.salary_date = DateTime.now + 36.month }
      
      it { expect(employee.salary).to eq 2725.0 }
    end

    context 'employee is working less than 1 year' do
      let(:working) { 0.year }
      
      it { expect(employee.salary).to eq 2500.0 }
    end

    context 'employee is working for 1 year' do
      let(:working) { 1.year }
      
      it { expect(employee.salary).to eq 2575.0 }
    end
  
    context 'employee is working for 5 years' do
      let(:working) { 5.years }
      
      it { expect(employee.salary).to eq 2875.0 }
    end

    context 'employee is working for 50 years' do
      let(:working) { 50.years }
      
      it { expect(employee.salary).to eq 3250.0 }
    end
  end
end

