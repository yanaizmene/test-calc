require 'spec_helper'
require 'active_support'
require 'active_support/core_ext'

require_relative '../../classes/manager_employee'

RSpec.describe ManagerEmployee do
  #base rate is 2500, bonus is 5% for each year, max bonus is 40% of base rate

  describe '.salary' do
    let(:employee) do
      chief = described_class.new('Scott', DateTime.now - working) 
      chief.add_subordinate(subordinate)

      chief
    end

    let(:subordinate)           { GenericEmployee.new('Fred', DateTime.now - 1.year) }
    let(:base_rate)             { 2500.0 }
    let(:bonus_rate_percentage) { 5.0 }

    context 'employee is working less than 1 year and has 3 subordinates' do
      let(:working) { 0.year }
      
      before do
        employee.add_subordinate(GenericEmployee.new('Samantha',  DateTime.now - 3.years))
        employee.add_subordinate(GenericEmployee.new('Lora',      DateTime.now - 6.years))
      end

      it { expect(employee.salary).to eq base_rate + 8250 * 0.5 / 100 }
    end

    context 'employee is working 4 years and has 3 subordinates' do
      let(:working) { 4.years }
      
      before do
        employee.add_subordinate(GenericEmployee.new('Samantha',  DateTime.now - 3.years))
        employee.add_subordinate(GenericEmployee.new('Lora',      DateTime.now - 6.years))
      end

      it { expect(employee.salary).to eq base_rate + (4 * bonus_rate_percentage / 100 * base_rate) + 8250 * 0.5 / 100 }
    end

    context 'employee is working for 1 year and has 1 subordinate' do
      let(:working) { 1.year }
      
      it { expect(employee.salary).to eq 12.875 + base_rate + (1 * bonus_rate_percentage / 100 * base_rate) }
    end
  end
end

