require 'spec_helper'
require 'active_support'
require 'active_support/core_ext'

require_relative '../../classes/sales_employee'
require_relative '../../classes/manager_employee'

RSpec.describe SalesEmployee do
  #base rate is 2500, bonus is 1% for each year, max bonus is 35% of base rate

  describe '.salary' do
    let(:employee) do
      described_class.new('Scott', DateTime.now - working)
    end

    context 'employee is working less than 1 year and has 3 subordinates' do
      let(:working) { 0.year }
      
      before do
        sub_chief   = ManagerEmployee.new('Samantha',   DateTime.now - 1.year)        
        sub_chief2  = ManagerEmployee.new('Sub Chief',  DateTime.now - 2.years)
        generic     = GenericEmployee.new('Lora',       DateTime.now - 1.year)
        
        sub_chief2.add_subordinate  generic
        sub_chief.add_subordinate   sub_chief2
        employee.add_subordinate    sub_chief
      end

      it { expect(employee.salary).to eq 2523.930068125}
    end
  end
end